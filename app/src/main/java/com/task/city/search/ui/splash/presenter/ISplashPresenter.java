package com.task.city.search.ui.splash.presenter;

import com.task.city.search.data.model.UserInfo;
import com.task.city.search.di.scope.PerActivity;
import com.task.city.search.ui.base.presenter.IBasePresenter;
import com.task.city.search.ui.splash.interactor.ISplashInteractor;
import com.task.city.search.ui.splash.view.SplashView;

@PerActivity
public interface ISplashPresenter<V extends SplashView,
        I extends ISplashInteractor> extends IBasePresenter<V, I> {
    UserInfo getCurrentUser();

}
