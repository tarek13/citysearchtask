package com.task.city.search.di.component;




import com.task.city.search.di.module.ActivityModule;
import com.task.city.search.di.scope.PerActivity;
import com.task.city.search.ui.login.LoginActivity;
import com.task.city.search.ui.main.MainActivity;
import com.task.city.search.ui.splash.SplashActivity;


import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class})
public interface ActivityComponent {

   void inject(MainActivity mainActivity);
   void inject(SplashActivity splashActivity);
   void inject(LoginActivity loginActivity);
}
