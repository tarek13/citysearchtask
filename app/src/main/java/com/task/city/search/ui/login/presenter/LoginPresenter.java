package com.task.city.search.ui.login.presenter;

import com.task.city.search.ui.base.presenter.BasePresenter;
import com.task.city.search.ui.login.interactor.ILoginInteractor;
import com.task.city.search.ui.login.view.LoginView;
import com.task.city.search.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class LoginPresenter<V extends LoginView, I extends ILoginInteractor>
        extends BasePresenter<V, I> implements ILoginPresenter<V, I> {


    @Inject
    public LoginPresenter(I mvpInteractor,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


}
