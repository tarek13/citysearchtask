package com.task.city.search.ui.main.presenter;

import com.task.city.search.R;
import com.task.city.search.data.network.model.CityItemResponse;
import com.task.city.search.data.network.model.FacebookUserInfo;
import com.task.city.search.data.network.model.GoogleUserInfo;
import com.task.city.search.ui.base.presenter.BasePresenter;
import com.task.city.search.ui.main.interactor.IMainInteractor;
import com.task.city.search.ui.main.view.MainView;
import com.task.city.search.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

public class MainPresenter<V extends MainView, I extends IMainInteractor>
        extends BasePresenter<V, I> implements IMainPresenter<V, I> {

    private Integer notificationsCount;

    @Inject
    public MainPresenter(I mvpInteractor,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void getCities() {
        getMvpView().showLoadingProgressbar();

        getCompositeDisposable().add(getInteractor().getCities()
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<ArrayList<CityItemResponse>>() {
                    @Override
                    public void accept(ArrayList<CityItemResponse> cityItemResponseArrayList) {
                        getMvpView().hideLoadingProgressbar();

                        getMvpView().showCities(cityItemResponseArrayList);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideLoadingProgressbar();
                        String userId="not login";
                        if(getInteractor().getPreferencesHelper().getCurrentUser()!=null){
                            userId= String.valueOf(getInteractor().getPreferencesHelper().getCurrentUser().getId());
                        }
                        if (!getInteractor().checkNetworkConnection()) {
                            getMvpView().showErrorMessage(R.string.error_msg_no_internet,R.drawable.no_wifi);
                            // logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage","","NO INTERNET CONNECTION",throwable.getMessage());

                        } else if (throwable instanceof TimeoutException) {
                            getMvpView().showErrorMessage(R.string.error_msg_timeout,R.drawable.no_wifi);
                            //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage","","SOCKET TIMEOUT",throwable.getMessage());

                        } else {
                            if (throwable instanceof HttpException) {
                                // getMvpView().onError(((HttpException) throwable).message());
                                if (((HttpException) throwable).response().code() == 400 || ((HttpException) throwable).response().code() == 404 || ((HttpException) throwable).response().code() == 422) {
                                    // toDO handle error

                                        getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);

                                }
                                else if (((HttpException) throwable).response().code() == 500) {
                                    getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                                    //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage", String.valueOf(((HttpException) throwable).response().code()),((HttpException) throwable).response().message(),throwable.getMessage());

                                }
                                else if((((HttpException) throwable).response().code() == 401)){
                                    getMvpView().openActivityOnTokenExpire();
                                }
                                else {
                                    getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);
                                    //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage", String.valueOf(((HttpException) throwable).response().code()),((HttpException) throwable).response().message(),throwable.getMessage());

                                }
                            } else {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                                //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage", "","",throwable.getMessage());


                            }
                        }
                    }
                }));
    }

    @Override
    public void getGoogleUserInfo(String token) {
        getMvpView().showMenuLoadingProgressbar();

        getCompositeDisposable().add(getInteractor().getGoogleUserInfo(token)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<GoogleUserInfo>() {
                    @Override
                    public void accept(GoogleUserInfo googleUserInfo) {
                        getMvpView().hideMenuLoadingProgressbar();

                       getMvpView().showUserInfo(googleUserInfo.getName(),googleUserInfo.getEmail(),googleUserInfo.getPicture());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideMenuLoadingProgressbar();
                        String userId="not login";
                        if(getInteractor().getPreferencesHelper().getCurrentUser()!=null){
                            userId= String.valueOf(getInteractor().getPreferencesHelper().getCurrentUser().getId());
                        }
                        if (!getInteractor().checkNetworkConnection()) {
                            getMvpView().showErrorMessage(R.string.error_msg_no_internet,R.drawable.no_wifi);
                            // logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage","","NO INTERNET CONNECTION",throwable.getMessage());

                        } else if (throwable instanceof TimeoutException) {
                            getMvpView().showErrorMessage(R.string.error_msg_timeout,R.drawable.no_wifi);
                            //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage","","SOCKET TIMEOUT",throwable.getMessage());

                        } else {
                            if (throwable instanceof HttpException) {
                                // getMvpView().onError(((HttpException) throwable).message());
                                if (((HttpException) throwable).response().code() == 400 || ((HttpException) throwable).response().code() == 404 || ((HttpException) throwable).response().code() == 422) {
                                    // toDO handle error

                                    getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);

                                }
                                else if (((HttpException) throwable).response().code() == 500) {
                                    getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                                    //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage", String.valueOf(((HttpException) throwable).response().code()),((HttpException) throwable).response().message(),throwable.getMessage());

                                }
                                else if((((HttpException) throwable).response().code() == 401)){
                                    getMvpView().openActivityOnTokenExpire();
                                }
                                else {
                                    getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);
                                    //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage", String.valueOf(((HttpException) throwable).response().code()),((HttpException) throwable).response().message(),throwable.getMessage());

                                }
                            } else {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                                //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage", "","",throwable.getMessage());


                            }
                        }
                    }
                }));
    }

    @Override
    public void getFaceBookUserInfo(String token) {
        getMvpView().showMenuLoadingProgressbar();

        getCompositeDisposable().add(getInteractor().getFaceBookUserInfo(token)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<FacebookUserInfo>() {
                    @Override
                    public void accept(FacebookUserInfo googleUserInfo) {
                        getMvpView().hideMenuLoadingProgressbar();

                       getMvpView().showUserInfo(googleUserInfo.getName(),googleUserInfo.getEmail(),googleUserInfo.getPicture().getData().getUrl());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        getMvpView().hideMenuLoadingProgressbar();
                        String userId="not login";
                        if(getInteractor().getPreferencesHelper().getCurrentUser()!=null){
                            userId= String.valueOf(getInteractor().getPreferencesHelper().getCurrentUser().getId());
                        }
                        if (!getInteractor().checkNetworkConnection()) {
                            getMvpView().showErrorMessage(R.string.error_msg_no_internet,R.drawable.no_wifi);
                            // logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage","","NO INTERNET CONNECTION",throwable.getMessage());

                        } else if (throwable instanceof TimeoutException) {
                            getMvpView().showErrorMessage(R.string.error_msg_timeout,R.drawable.no_wifi);
                            //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage","","SOCKET TIMEOUT",throwable.getMessage());

                        } else {
                            if (throwable instanceof HttpException) {
                                // getMvpView().onError(((HttpException) throwable).message());
                                if (((HttpException) throwable).response().code() == 400 || ((HttpException) throwable).response().code() == 404 || ((HttpException) throwable).response().code() == 422) {
                                    // toDO handle error

                                    getMvpView().showErrorMessage(R.string.some_error, R.drawable.no_wifi);

                                }
                                else if (((HttpException) throwable).response().code() == 500) {
                                    getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                                    //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage", String.valueOf(((HttpException) throwable).response().code()),((HttpException) throwable).response().message(),throwable.getMessage());

                                }
                                else if((((HttpException) throwable).response().code() == 401)){
                                    getMvpView().openActivityOnTokenExpire();
                                }
                                else {
                                    getMvpView().showErrorMessage(R.string.connection_error,R.drawable.no_wifi);
                                    //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage", String.valueOf(((HttpException) throwable).response().code()),((HttpException) throwable).response().message(),throwable.getMessage());

                                }
                            } else {
                                getMvpView().showErrorMessage(R.string.connection_error, R.drawable.no_wifi);
                                //  logEvent(userId, "YOUR_STORE_LIST_FRAGMENT","loadFirstPage", "","",throwable.getMessage());


                            }
                        }
                    }
                }));
    }
}
