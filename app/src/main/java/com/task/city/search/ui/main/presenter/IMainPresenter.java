package com.task.city.search.ui.main.presenter;

import com.task.city.search.di.scope.PerActivity;
import com.task.city.search.ui.base.presenter.IBasePresenter;
import com.task.city.search.ui.main.interactor.IMainInteractor;
import com.task.city.search.ui.main.view.MainView;

@PerActivity
public interface IMainPresenter<V extends MainView, I extends IMainInteractor> extends IBasePresenter<V, I> {

    void getCities();

    void getGoogleUserInfo(String token);

    void getFaceBookUserInfo(String token);
}
