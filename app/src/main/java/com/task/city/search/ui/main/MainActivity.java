package com.task.city.search.ui.main;

import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.task.city.search.CitySearchApp;
import com.task.city.search.GlideApp;
import com.task.city.search.R;
import com.task.city.search.data.network.model.CityItemResponse;
import com.task.city.search.di.component.ActivityComponent;
import com.task.city.search.di.component.DaggerActivityComponent;
import com.task.city.search.di.module.ActivityModule;
import com.task.city.search.di.module.NetworkModule;
import com.task.city.search.ui.base.BaseActivity;
import com.task.city.search.ui.main.adapter.CitiesListAdapter;
import com.task.city.search.ui.main.interactor.IMainInteractor;
import com.task.city.search.ui.main.presenter.IMainPresenter;
import com.task.city.search.ui.main.view.MainView;
import com.task.city.search.utils.AppConstants;
import com.task.city.search.utils.ChangeBaseUrlInterceptorWithLogging;
import com.task.city.search.utils.IdentityProvider;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationResponse;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceDiscovery;
import net.openid.appauth.TokenRequest;
import net.openid.appauth.TokenResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.task.city.search.utils.AppConstants.EXTRA_AUTH_SERVICE_DISCOVERY;
import static com.task.city.search.utils.AppConstants.EXTRA_CLIENT_SECRET;
import static com.task.city.search.utils.AppConstants.KEY_AUTH_STATE;
import static com.task.city.search.utils.AppConstants.KEY_USER_INFO;

public class MainActivity extends BaseActivity implements MainView, CitiesListAdapter.Callback {
    private static final String TAG = "MainActivity";

    @BindView(R.id.main_error)
    TextView mainError;
    @BindView(R.id.progress_bar_container)
    LinearLayout progressBarContainer;
    @BindView(R.id.main_city_recycler_view)
    RecyclerView mainCityRecyclerView;
    @Inject
    CitiesListAdapter citiesListAdapter;
    @BindView(R.id.main_container)
    RelativeLayout mainContainer;
    @BindView(R.id.main_nav_menu_view_image_view)
    CircleImageView mainNavMenuViewImageView;
    @BindView(R.id.main_nav_menu_name_text_view)
    TextView mainNavMenuNameTextView;
    @BindView(R.id.main_nav_menu_email_text_view)
    TextView mainNavMenuEmailTextView;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.mein_menu_container)
    RelativeLayout meinMenuContainer;
    @BindView(R.id.main_menu_error)
    TextView mainMenuError;
    @BindView(R.id.progress_bar_menu_container)
    LinearLayout progressBarMenuContainer;


    @Inject
    IMainPresenter<MainView, IMainInteractor> mainPresenter;

    @Inject
    ChangeBaseUrlInterceptorWithLogging changeBaseUrlInterceptorWithLogging;


    ActivityComponent mainComponent;



    private ActionBarDrawerToggle mDrawerToggle;


    private SearchView searchView;



    private AuthState mAuthState;
    private AuthorizationService mAuthService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUnBinder(ButterKnife.bind(this));
        mainComponent = DaggerActivityComponent.builder()
                .applicationComponent(((CitySearchApp) getApplication()).getComponent())
                .networkModule(new NetworkModule(AppConstants.BASE_URL))
                .activityModule(new ActivityModule(this))
                .build();


        mainComponent.inject(this);

        mainPresenter.onAttach(this);

        setupAuth();
        setUp();


    }

    private void setupAuth() {
        mAuthService = new AuthorizationService(this);

        if (mAuthState == null) {
            AuthorizationResponse response = AuthorizationResponse.fromIntent(getIntent());
            AuthorizationException ex = AuthorizationException.fromIntent(getIntent());
            mAuthState = new AuthState(response, ex);

            if (response != null) {
                Log.d(TAG, "Received AuthorizationResponse.");
                showSnackbar(R.string.exchange_notification);
                exchangeAuthorizationCode(response);
            } else {
                Log.i(TAG, "Authorization failed: " + ex);
                showSnackbar(R.string.authorization_failed);
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
    }



    @Override
    protected void setUp() {
        setSupportActionBar(toolbar);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mainCityRecyclerView.setLayoutManager(linearLayoutManager);
        mainCityRecyclerView.setAdapter(citiesListAdapter);
        citiesListAdapter.setCallback(this);
        mainCityRecyclerView.setItemAnimator(new DefaultItemAnimator());
        changeBaseUrlInterceptorWithLogging.setInterceptor(AppConstants.BASE_URL);
          mainPresenter.getCities();


    }


    @Override
    protected void onDestroy() {
        mainPresenter.onDetach();
        mAuthService.dispose();
        super.onDestroy();
    }


    @Override
    public void onDeleteButtonClick(int position, File file) {

    }

    @Override
    public void showLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.VISIBLE);
            mainCityRecyclerView.setVisibility(View.GONE);
            mainError.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideLoadingProgressbar() {
        if (progressBarContainer != null) {
            progressBarContainer.setVisibility(View.GONE);
            mainCityRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showErrorMessage(int messageID, int errorIcon) {
        mainCityRecyclerView.setVisibility(View.GONE);
        mainError.setVisibility(View.VISIBLE);
        mainError.setText(messageID);
        mainError.setCompoundDrawablesRelativeWithIntrinsicBounds(0, errorIcon, 0, 0);
    }

    @Override
    public void showMenuLoadingProgressbar() {
        if (progressBarMenuContainer != null) {
            progressBarMenuContainer.setVisibility(View.VISIBLE);
            meinMenuContainer.setVisibility(View.GONE);
            mainMenuError.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideMenuLoadingProgressbar() {
        if (progressBarMenuContainer != null) {
            progressBarMenuContainer.setVisibility(View.GONE);
            meinMenuContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showEuMenrrorMessage(int messageID, int errorIcon) {
        meinMenuContainer.setVisibility(View.GONE);
        mainMenuError.setVisibility(View.GONE);
        mainMenuError.setText(messageID);
        mainMenuError.setCompoundDrawablesRelativeWithIntrinsicBounds(0, errorIcon, 0, 0);
    }

    @Override
    public void showCities(ArrayList<CityItemResponse> cityItemResponseArrayList) {
        if (cityItemResponseArrayList != null && cityItemResponseArrayList.size() > 0) {
            //  locationsResultTextView.setText(addLocationItemResponses.size()+" "+getString(R.string.result));
            mainCityRecyclerView.setVisibility(View.VISIBLE);
            mainError.setVisibility(View.GONE);
            citiesListAdapter.setData(cityItemResponseArrayList);
        } else {
            //  locationsResultTextView.setText(0+" "+getString(R.string.result));
            showErrorMessage(R.string.cities_error, R.drawable.no_wifi);
        }
    }

    @Override
    public void showUserInfo(String name, String email, String picture) {
        mainNavMenuNameTextView.setText(name);
        mainNavMenuEmailTextView.setText(email);
        GlideApp.with(this).load(Uri.parse(picture)).placeholder(R.drawable.ic_bg).error(R.drawable.ic_bg).into(mainNavMenuViewImageView);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                citiesListAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                citiesListAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    public static PendingIntent createPostAuthorizationIntent(
            @NonNull Context context,
            @NonNull AuthorizationRequest request,
            @Nullable AuthorizationServiceDiscovery discoveryDoc,
            @Nullable String clientSecret) {
        Intent intent = new Intent(context, MainActivity.class);
        if (discoveryDoc != null) {
            intent.putExtra(EXTRA_AUTH_SERVICE_DISCOVERY, discoveryDoc.docJson.toString());
        }

        if (clientSecret != null) {
            intent.putExtra(EXTRA_CLIENT_SECRET, clientSecret);
        }
        return PendingIntent.getActivity(context, request.hashCode(), intent, 0);
    }

    @MainThread
    private void showSnackbar(@StringRes int messageId) {
        /*Snackbar.make(mainContainer,
                getResources().getString(messageId),
                Snackbar.LENGTH_SHORT)
                .show();*/
    }

    private void exchangeAuthorizationCode(AuthorizationResponse authorizationResponse) {
        HashMap<String, String> additionalParams = new HashMap<>();
        if (getClientSecretFromIntent(getIntent()) != null) {
            additionalParams.put("client_secret", getClientSecretFromIntent(getIntent()));
        }
        performTokenRequest(authorizationResponse.createTokenExchangeRequest(additionalParams));
    }

    private String getClientSecretFromIntent(Intent intent) {
        if (!intent.hasExtra(EXTRA_CLIENT_SECRET)) {
            return null;
        }
        return intent.getStringExtra(EXTRA_CLIENT_SECRET);
    }

    private void performTokenRequest(TokenRequest request) {
        mAuthService.performTokenRequest(
                request,
                new AuthorizationService.TokenResponseCallback() {
                    @Override
                    public void onTokenRequestCompleted(
                            @Nullable TokenResponse tokenResponse,
                            @Nullable AuthorizationException ex) {
                        receivedTokenResponse(tokenResponse, ex);
                    }
                });
    }

    private void receivedTokenResponse(
            @Nullable TokenResponse tokenResponse,
            @Nullable AuthorizationException authException) {
        Log.d(TAG, "Token request complete");
        mAuthState.update(tokenResponse, authException);
        showSnackbar((tokenResponse != null)
                ? R.string.exchange_complete
                : R.string.refresh_failed);
        fetchUserInfo();

    }



    private void fetchUserInfo() {
        if (mAuthState.getAuthorizationServiceConfiguration() == null) {
            Log.e(TAG, "Cannot make userInfo request without service configuration");
        }

        mAuthState.performActionWithFreshTokens(mAuthService, new AuthState.AuthStateAction() {
            @Override
            public void execute(String accessToken, String idToken, AuthorizationException ex) {
                if (ex != null) {
                    Log.e(TAG, "Token refresh failed when fetching user info");
                    return;
                }
                AuthorizationServiceDiscovery discoveryDoc = getDiscoveryDocFromIntent(getIntent());
                if(discoveryDoc== null){
                    changeBaseUrlInterceptorWithLogging.setInterceptor("https://graph.facebook.com/");
                    mainPresenter.getFaceBookUserInfo("Bearer "+mAuthState.getAccessToken());
                }else {
                    changeBaseUrlInterceptorWithLogging.setInterceptor("https://openidconnect.googleapis.com/");
                    mainPresenter.getGoogleUserInfo("Bearer "+mAuthState.getAccessToken());
                }
            }
        });
    }



    private AuthorizationServiceDiscovery getDiscoveryDocFromIntent(Intent intent) {
        if (!intent.hasExtra(EXTRA_AUTH_SERVICE_DISCOVERY)) {
            return null;
        }
        String discoveryJson = intent.getStringExtra(EXTRA_AUTH_SERVICE_DISCOVERY);
        try {
            return new AuthorizationServiceDiscovery(new JSONObject(discoveryJson));
        } catch (JSONException | AuthorizationServiceDiscovery.MissingArgumentException ex) {
            ex.printStackTrace();
            return null;
        }
    }



}
