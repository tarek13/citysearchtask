package com.task.city.search.ui.login.interactor;

import com.task.city.search.data.model.UserInfo;
import com.task.city.search.ui.base.interactor.IBaseInteractor;

public interface ILoginInteractor extends IBaseInteractor {


    void saveUserToSharedPreferences(UserInfo userInfo);


}
