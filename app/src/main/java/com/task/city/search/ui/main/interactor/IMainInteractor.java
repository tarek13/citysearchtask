package com.task.city.search.ui.main.interactor;

import com.task.city.search.data.network.model.CityItemResponse;
import com.task.city.search.data.network.model.FacebookUserInfo;
import com.task.city.search.data.network.model.GoogleUserInfo;
import com.task.city.search.ui.base.interactor.IBaseInteractor;

import java.util.ArrayList;

import io.reactivex.Observable;

public interface IMainInteractor extends IBaseInteractor {


    Observable<ArrayList<CityItemResponse>> getCities();

    Observable<GoogleUserInfo> getGoogleUserInfo(String token);

    Observable<FacebookUserInfo> getFaceBookUserInfo(String token);
}
