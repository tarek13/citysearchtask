package com.task.city.search.ui.main.interactor;

import android.content.Context;

import com.task.city.search.data.network.CitiesApis;
import com.task.city.search.data.network.model.CityItemResponse;
import com.task.city.search.data.network.model.FacebookUserInfo;
import com.task.city.search.data.network.model.GoogleUserInfo;
import com.task.city.search.data.prefs.PreferencesHelper;
import com.task.city.search.di.qualifier.ActivityContext;
import com.task.city.search.ui.base.interactor.BaseInteractor;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;

public class MainInteractor extends BaseInteractor implements IMainInteractor {

    private Context mContext;
    private CitiesApis citiesApis;

    @Inject
    public MainInteractor(@ActivityContext Context context, PreferencesHelper preferencesHelper, CitiesApis citiesApis) {
        super(context,preferencesHelper);
        mContext=context;
        this.citiesApis=citiesApis;
    }


    @Override
    public Observable<ArrayList<CityItemResponse>> getCities() {
        return citiesApis.getCities();
    }

    @Override
    public Observable<GoogleUserInfo> getGoogleUserInfo(String token) {
        return citiesApis.getGoogleUserInfo(token);
    }

    @Override
    public Observable<FacebookUserInfo> getFaceBookUserInfo(String token) {
        return citiesApis.getFaceBookUserInfo(token,"id,name,email,picture.type(large)");
    }
}
