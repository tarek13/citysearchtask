package com.task.city.search.ui.main.view;

import com.task.city.search.data.network.model.CityItemResponse;
import com.task.city.search.ui.base.view.IBaseView;

import java.util.ArrayList;

public interface MainView extends IBaseView {


    void showLoadingProgressbar();

    void hideLoadingProgressbar();

    void showErrorMessage(int messageID, int errorIcon);


    void showMenuLoadingProgressbar();

    void hideMenuLoadingProgressbar();

    void showEuMenrrorMessage(int messageID, int errorIcon);

    void showCities(ArrayList<CityItemResponse> cityItemResponseArrayList);

    void showUserInfo(String name, String email, String picture);
}
