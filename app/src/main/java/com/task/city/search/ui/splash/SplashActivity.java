package com.task.city.search.ui.splash;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;

import com.task.city.search.CitySearchApp;
import com.task.city.search.R;
import com.task.city.search.data.model.UserInfo;
import com.task.city.search.di.component.ActivityComponent;
import com.task.city.search.di.component.DaggerActivityComponent;
import com.task.city.search.di.module.ActivityModule;
import com.task.city.search.di.module.NetworkModule;
import com.task.city.search.ui.base.BaseActivity;
import com.task.city.search.ui.login.LoginActivity;
import com.task.city.search.ui.main.MainActivity;
import com.task.city.search.ui.splash.interactor.ISplashInteractor;
import com.task.city.search.ui.splash.presenter.ISplashPresenter;
import com.task.city.search.ui.splash.view.SplashView;
import com.task.city.search.utils.AppConstants;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashView {

    private static final String TAG = "SplashActivity";
    private static final int RC_SIGN_IN = 13;

    @Inject
    ISplashPresenter<SplashView, ISplashInteractor> mPresenter;

    ActivityComponent splashComponent;

    private UserInfo userInfo;
    private String voteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setUnBinder(ButterKnife.bind(this));

        // define splash component of DI
        splashComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .networkModule(new NetworkModule(AppConstants.BASE_URL))
                .applicationComponent(((CitySearchApp) getApplication()).getComponent())
                .build();

        //  to tell the Dagger to scan this class through the implementation of this interface.
        splashComponent.inject(this);
        mPresenter.onAttach(SplashActivity.this);
        userInfo = mPresenter.getCurrentUser();
        setUp();
       // handleIntent(getIntent());
    }

    @Override
    protected void setUp() {
        startDelayedThread();

    }

    /*
     * open login activity
     * */
    @Override
    public void openLoginActivity() {
        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    /*
     * open main activity
     * */
    @Override
    public void openMainActivity() {
        hideKeyboard();
        Intent intentMainActivity = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intentMainActivity);
        finish();
    }




    @Override
    public void doLogin() {
        if(userInfo!=null) {

        }else {
            startDelayedThread();
        }
    }

    /*
     * start delayed thread to open login activty or main activity after 2000ms
     * */
    @Override
    public void startDelayedThread() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app ic_logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                openLoginActivity();
                // close this activity
                finish();
            }
        }, AppConstants.SPLASH_TIME_OUT);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }


    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            try {
                byte voteIdBytes[] = Base64.decode(appLinkData.getLastPathSegment().getBytes(), Base64.DEFAULT);
                voteId= new String(voteIdBytes);
            }catch (Exception e){
                e.printStackTrace();
                voteId=null;
            }

           // showRecipe(appData);
        }
    }

}
