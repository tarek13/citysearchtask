package com.task.city.search.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.task.city.search.CitySearchApp;
import com.task.city.search.R;
import com.task.city.search.di.component.ActivityComponent;
import com.task.city.search.di.component.DaggerActivityComponent;
import com.task.city.search.di.module.ActivityModule;
import com.task.city.search.di.module.NetworkModule;
import com.task.city.search.ui.base.BaseActivity;
import com.task.city.search.ui.login.interactor.ILoginInteractor;
import com.task.city.search.ui.login.presenter.ILoginPresenter;
import com.task.city.search.ui.login.view.LoginView;
import com.task.city.search.ui.main.MainActivity;
import com.task.city.search.utils.AppConstants;
import com.task.city.search.utils.IdentityProvider;

import net.openid.appauth.AuthorizationException;
import net.openid.appauth.AuthorizationRequest;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.ResponseTypeValues;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView {

    private static final int RC_SIGN_IN = 13;


    private static final String TAG = "LoginActivity";
    @BindView(R.id.login_email_edit_text)
    TextInputEditText loginEmailEditText;
    @BindView(R.id.login_email_text_input)
    TextInputLayout loginEmailTextInput;
    @BindView(R.id.login_password_edit_text)
    TextInputEditText loginPasswordEditText;
    @BindView(R.id.login_password_text_input)
    TextInputLayout loginPasswordTextInput;
    @BindView(R.id.login_facebook_button)
    ImageView loginFacebookButton;
    @BindView(R.id.login_google_button)
    ImageView loginGoogleButton;
    @BindView(R.id.login_social_container)
    LinearLayout loginSocialContainer;
    @BindView(R.id.login_sign_in_buttun)
    TextView loginSignInButtun;

    @Inject
    ILoginPresenter<LoginView, ILoginInteractor> mPresenter;


    ActivityComponent loginComponent;

    private AuthorizationService mAuthService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setUnBinder(ButterKnife.bind(this));


        loginComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .networkModule(new NetworkModule(AppConstants.BASE_URL))
                .applicationComponent(((CitySearchApp) getApplication()).getComponent()).build();

        loginComponent.inject(this);

        mPresenter.onAttach(this);

        setUp();

    }


    //function to setup view before start


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        finish();

    }

    @Override
    protected void setUp() {
        mAuthService = new AuthorizationService(this);
        List<IdentityProvider> providers = IdentityProvider.getEnabledProviders(this);

       /* for (final IdentityProvider idp : providers) {


            Button idpButton = new Button(this, null, R.style.Widget_AppCompat_Button_Borderless);
            idpButton.setBackgroundResource(idp.buttonImageRes);
            idpButton.setContentDescription(
                    getResources().getString(idp.buttonContentDescriptionRes));
            idpButton.setWidth(getResources().getDimensionPixelSize(R.dimen.idp_button_edge_size));
            idpButton.setHeight(getResources().getDimensionPixelSize(R.dimen.idp_button_edge_size));
            idpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "initiating auth for " + idp.name);
                    idp.retrieveConfig(LoginActivity.this, retrieveCallback);
                }
            });

        }*/

    }



    @Override
    public void openMainActivity(String message) {

        hideKeyboard();
        // showMessage(message, Toast.LENGTH_LONG);
        Intent intentMainActivity = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intentMainActivity);
        finish();

    }

    @OnClick({R.id.login_facebook_button, R.id.login_google_button,R.id.login_sign_in_buttun})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_facebook_button:
                IdentityProvider.FACEBOOK.retrieveConfig(LoginActivity.this,  iniliazeAuthRequest(IdentityProvider.FACEBOOK));
                break;
            case R.id.login_google_button:
                IdentityProvider.GOOGLE.retrieveConfig(LoginActivity.this,  iniliazeAuthRequest(IdentityProvider.GOOGLE));
                break;
                case R.id.login_sign_in_buttun:
                    openMainActivity("");
                break;
        }
    }

    private  AuthorizationServiceConfiguration.RetrieveConfigurationCallback iniliazeAuthRequest(final IdentityProvider idp){
        final AuthorizationServiceConfiguration.RetrieveConfigurationCallback retrieveCallback =
                new AuthorizationServiceConfiguration.RetrieveConfigurationCallback() {

                    @Override
                    public void onFetchConfigurationCompleted(
                            @Nullable AuthorizationServiceConfiguration serviceConfiguration,
                            @Nullable AuthorizationException ex) {
                        if (ex != null) {
                            Log.w(TAG, "Failed to retrieve configuration for " + idp.name, ex);
                        } else {
                            Log.d(TAG, "configuration retrieved for " + idp.name
                                    + ", proceeding");
                            makeAuthRequest(serviceConfiguration, idp);
                        }
                    }
                };
        return retrieveCallback;
    }
    private void makeAuthRequest(
            @NonNull AuthorizationServiceConfiguration serviceConfig,
            @NonNull IdentityProvider idp) {

        AuthorizationRequest authRequest = new AuthorizationRequest.Builder(
                serviceConfig,
                idp.getClientId(),
                ResponseTypeValues.CODE,
                idp.getRedirectUri())
                .setScope(idp.getScope())
                .build();

        Log.d(TAG, "Making auth request to " + idp.name);
        mAuthService.performAuthorizationRequest(
                authRequest,
                MainActivity.createPostAuthorizationIntent(
                        this,
                        authRequest,
                        serviceConfig.discoveryDoc,
                        idp.getClientSecret()),
                mAuthService.createCustomTabsIntentBuilder()
                        .build());
    }
}
