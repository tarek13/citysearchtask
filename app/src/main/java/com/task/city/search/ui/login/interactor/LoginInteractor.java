package com.task.city.search.ui.login.interactor;

import android.content.Context;

import com.task.city.search.data.model.UserInfo;
import com.task.city.search.data.prefs.PreferencesHelper;
import com.task.city.search.di.qualifier.ActivityContext;
import com.task.city.search.ui.base.interactor.BaseInteractor;
import com.task.city.search.utils.NetworkUtils;

import javax.inject.Inject;

public class LoginInteractor extends BaseInteractor implements ILoginInteractor {

    private Context mContext;

    @Inject
    public LoginInteractor(@ActivityContext Context context,
                           PreferencesHelper preferencesHelper) {
        super(context,preferencesHelper);
        mContext=context;

    }



    @Override
    public void saveUserToSharedPreferences(UserInfo userInfo) {
        getPreferencesHelper().saveCurrentUserInfo(userInfo);
    }


    @Override
    public boolean checkNetworkConnection() {
        return NetworkUtils.isNetworkConnected(mContext);
    }

}
