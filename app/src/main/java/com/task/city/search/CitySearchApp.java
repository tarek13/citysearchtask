package com.task.city.search;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import androidx.multidex.MultiDex;

import com.task.city.search.di.component.ApplicationComponent;
import com.task.city.search.di.component.DaggerApplicationComponent;
import com.task.city.search.di.module.ApplicationModule;
import com.task.city.search.utils.LocaleManager;

import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

public class CitySearchApp extends Application {

    private ApplicationComponent mApplicationComponent;



    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);


        try {
            SSLContext.getInstance("TLSv1.2");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/Calibri.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());


    }


    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.setLocale(this);
        // Log.d(TAG, "onConfigurationChanged: " + newConfig.locale.getLanguage());
    }
}
