package com.task.city.search.ui.login.view;

import com.task.city.search.ui.base.view.IBaseView;

public interface LoginView extends IBaseView {

    void openMainActivity(String s);

}
