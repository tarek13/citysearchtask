package com.task.city.search.ui.main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.task.city.search.R;
import com.task.city.search.data.network.model.CityItemResponse;
import com.task.city.search.ui.main.holder.CityItemViewHolder;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;


public class CitiesListAdapter extends RecyclerView.Adapter<CityItemViewHolder> implements Filterable {



    private ArrayList<CityItemResponse> cityItemResponses = new ArrayList<>();

    private Callback callback;

    private Context context;
    private ArrayList<CityItemResponse> citiesFiltered=new ArrayList<>();


    @Inject
    public CitiesListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    citiesFiltered = cityItemResponses;
                } else {
                    ArrayList<CityItemResponse> filteredList = new ArrayList<>();
                    for (CityItemResponse row : cityItemResponses) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getCountry().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    citiesFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = citiesFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                citiesFiltered = (ArrayList<CityItemResponse>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface Callback {
        void onDeleteButtonClick(int position, File file);
    }

    public void setData(ArrayList<CityItemResponse> cityItemResponseArrayList) {
        this.citiesFiltered = cityItemResponseArrayList;
        this.cityItemResponses = cityItemResponseArrayList;
        notifyDataSetChanged();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public CityItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CityItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_city_item, parent, false), context, callback);
    }

    @Override
    public void onBindViewHolder(@NonNull CityItemViewHolder holder, int position) {
        holder.setData(citiesFiltered.get(position), position);
    }


    @Override
    public int getItemCount() {
        return citiesFiltered.size();
    }


}
