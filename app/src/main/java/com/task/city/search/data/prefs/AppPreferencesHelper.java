/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.task.city.search.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.task.city.search.data.model.UserInfo;
import com.task.city.search.di.qualifier.ApplicationContext;
import com.task.city.search.di.qualifier.PreferenceInfo;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;



@Singleton
public class AppPreferencesHelper implements PreferencesHelper {



    private final SharedPreferences mPrefs;
    private String PREF_CURRENT_USER="PREF_CURRENT_USER";
    private static final String PREF_CURRENT_CATEGORY = "PREF_CURRENT_CATEGORY";
    private static final String PREF_CURRENT_MAIN_CATEGORY = "PREF_CURRENT_MAIN_CATEGORY";
    private static final String PREF_CURRENT_LANGUAGE = "PREF_CURRENT_LANGUAGE";
    private static final String PREF_CHANGE_LANGUAGE_FLAG = "PREF_CHANGE_LANGUAGE_FLAG";
    private static final String PREF_PLAYER_ID = "PREF_PLAYER_ID";
    private static final String PREF_IP_DATA_ENCODED = "PREF_IP_DATA_ENCODED";
    private static final String PREF_IP_DATA = "PREF_IP_DATA";
    private static final String PREF_APP_TOKEN = "PREF_APP_TOKEN";

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void saveCurrentUserInfo(UserInfo userInfo) {
        Gson gson = new Gson();
        String userInfoListJsonString = gson.toJson(userInfo);
        mPrefs.edit().putString(PREF_CURRENT_USER, userInfoListJsonString).apply();
    }

    @Override
    public UserInfo getCurrentUser() {
        Gson gson = new Gson();
        return gson.fromJson(mPrefs.getString(PREF_CURRENT_USER, ""), UserInfo.class);
    }

    @Override
    public void removeCurrentUser(){
        mPrefs.edit().remove(PREF_CURRENT_USER).apply();
    }

    @Override
    public void savePlayerId(String playerId) {
        mPrefs.edit().putString(PREF_PLAYER_ID, playerId).apply();
    }

    @Override
    public String getPlayerId() {
        return mPrefs.getString(PREF_PLAYER_ID,null);
    }

    @Override
    public void saveCurrentLanguage(String currentLanguage) {
        mPrefs.edit().putString(PREF_CURRENT_LANGUAGE, currentLanguage).apply();

    }

    @Override
    public String getCurrentLanguage() {
        return mPrefs.getString(PREF_CURRENT_LANGUAGE, "en");
    }

    @Override
    public void saveIpDataEncoded(String ipDataJsonEncoded) {
        mPrefs.edit().putString(PREF_IP_DATA_ENCODED, ipDataJsonEncoded).apply();

    }

    @Override
    public String getIpDataEncoded() {
        return mPrefs.getString(PREF_IP_DATA_ENCODED, null);
    }

    @Override
    public void saveIpData(String ipData) {
        mPrefs.edit().putString(PREF_IP_DATA, ipData).apply();

    }

    @Override
    public String getIpData() {
        return mPrefs.getString(PREF_IP_DATA, null);
    }

    @Override
    public void saveAppToken(String appToken) {
        mPrefs.edit().putString(PREF_APP_TOKEN, appToken).apply();
    }

    @Override
    public String getAppToken() {
        return mPrefs.getString(PREF_APP_TOKEN, null);
    }
}
