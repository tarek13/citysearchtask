package com.task.city.search.di.component;


import com.task.city.search.di.module.FragmentModule;
import com.task.city.search.di.scope.PerActivity;


import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {FragmentModule.class})
public interface FragmentComponent {




}
