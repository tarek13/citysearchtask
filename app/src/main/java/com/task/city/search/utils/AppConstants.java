

package com.task.city.search.utils;

import android.content.Context;

import com.task.city.search.R;

public final class AppConstants {

    public static final long SPLASH_TIME_OUT = 2000;

    public static final String BASE_URL = "https://raw.githubusercontent.com/";


    public static final String PREF_NAME = "alpha_pref";

    public static final String KEY_AUTH_STATE = "authState";
    public static final String KEY_USER_INFO = "userInfo";

    public static final String EXTRA_AUTH_SERVICE_DISCOVERY = "authServiceDiscovery";
    public static final String EXTRA_CLIENT_SECRET = "clientSecret";
    public static final int BUFFER_SIZE = 1024;




}

/*
        android:fontFamily="sans-serif"           // roboto regular
        android:fontFamily="sans-serif-light"     // roboto light
        android:fontFamily="sans-serif-condensed" // roboto condensed
        android:fontFamily="sans-serif-black"     // roboto black
        android:fontFamily="sans-serif-thin"      // roboto thin (android 4.2)
        android:fontFamily="sans-serif-medium"    // roboto medium (android 5.0)*/
