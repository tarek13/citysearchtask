/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.task.city.search.di.module;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.task.city.search.data.network.CitiesApis;
import com.task.city.search.di.qualifier.ActivityContext;
import com.task.city.search.di.scope.PerActivity;
import com.task.city.search.ui.login.interactor.ILoginInteractor;
import com.task.city.search.ui.login.interactor.LoginInteractor;
import com.task.city.search.ui.login.presenter.ILoginPresenter;
import com.task.city.search.ui.login.presenter.LoginPresenter;
import com.task.city.search.ui.login.view.LoginView;
import com.task.city.search.ui.main.adapter.CitiesListAdapter;
import com.task.city.search.ui.main.interactor.IMainInteractor;
import com.task.city.search.ui.main.interactor.MainInteractor;
import com.task.city.search.ui.main.presenter.IMainPresenter;
import com.task.city.search.ui.main.presenter.MainPresenter;
import com.task.city.search.ui.main.view.MainView;
import com.task.city.search.ui.splash.interactor.ISplashInteractor;
import com.task.city.search.ui.splash.interactor.SplashInteractor;
import com.task.city.search.ui.splash.presenter.ISplashPresenter;
import com.task.city.search.ui.splash.presenter.SplashPresenter;
import com.task.city.search.ui.splash.view.SplashView;
import com.task.city.search.utils.rx.AppSchedulerProvider;
import com.task.city.search.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;


@Module(includes = NetworkModule.class)
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    CitiesApis provideUserApis(Retrofit retrofit) {
        return retrofit.create(CitiesApis.class);
    }

    @Provides
    @PerActivity
    IMainPresenter<MainView, IMainInteractor> provideMainPresenter (MainPresenter<MainView,IMainInteractor> mainPresenter){
        return mainPresenter;
    }

    @Provides
    @PerActivity
    IMainInteractor provideMainInteractor (MainInteractor mainInteractor){
        return mainInteractor;
    }

    @Provides
    @PerActivity
    ISplashPresenter<SplashView, ISplashInteractor> provideSplashPresenter(
            SplashPresenter<SplashView, ISplashInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ISplashInteractor provideSplashMvpInteractor(SplashInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    ILoginPresenter<LoginView, ILoginInteractor> provideLoginPresenter (LoginPresenter<LoginView , ILoginInteractor> loginPresenter){
        return  loginPresenter;
    }

    @Provides
    @PerActivity
    ILoginInteractor provideLoginInteractor (LoginInteractor loginInteractor) {
        return  loginInteractor;
    }

    @Provides
    @PerActivity
    CitiesListAdapter provideMainMenuNavigationAdapter(@ActivityContext Context context){
        return new CitiesListAdapter(context);
    }


}
