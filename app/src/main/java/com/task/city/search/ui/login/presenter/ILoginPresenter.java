package com.task.city.search.ui.login.presenter;

import com.task.city.search.di.scope.PerActivity;
import com.task.city.search.ui.base.presenter.IBasePresenter;
import com.task.city.search.ui.login.interactor.ILoginInteractor;
import com.task.city.search.ui.login.view.LoginView;

@PerActivity
public interface  ILoginPresenter<V extends LoginView, I extends ILoginInteractor>
        extends IBasePresenter<V, I> {


}
