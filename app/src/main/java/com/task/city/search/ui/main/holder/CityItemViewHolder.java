package com.task.city.search.ui.main.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.task.city.search.R;
import com.task.city.search.data.network.model.CityItemResponse;
import com.task.city.search.ui.main.adapter.CitiesListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CityItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.city_logo_image_view)
    ImageView cityLogoImageView;
    @BindView(R.id.city_name_text_view)
    TextView cityNameTextView;
    @BindView(R.id.city_lat_lang_text_view)
    TextView cityLatLangTextView;
    @BindView(R.id.city_lat_text_view)
    TextView cityLatTextView;
    @BindView(R.id.city_long_text_view)
    TextView cityLongTextView;
    @BindView(R.id.city_container)
    CardView cityContainer;

    private CitiesListAdapter.Callback callback;

    private Context context;

    private CityItemResponse cityItemResponse;

    private int position;
    private long mLastClickTime;

    public CityItemViewHolder(View itemView, Context context, CitiesListAdapter.Callback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.context=context;
        this.callback=callback;


    }

    public void setData(CityItemResponse cityItemResponse, int position) {
        this.cityItemResponse = cityItemResponse;
        this.position=position;
        setup();
    }

    private void setup() {
        cityNameTextView.setText(cityItemResponse.getName());
        cityLatLangTextView.setText(cityItemResponse.getCountry());
        cityLatTextView.setText("Lat: "+cityItemResponse.getLat());
        cityLongTextView.setText("Lng: "+cityItemResponse.getLng());
    }

}
