

package com.task.city.search.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;

import com.afollestad.materialdialogs.MaterialDialog;
import com.task.city.search.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CommonUtils {

    private static final String TAG = "CommonUtils";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static MaterialDialog showLoadingDialog(Context context) {
       return new MaterialDialog.Builder(context)
                .content(R.string.please_wait)
                .cancelable(false)
                .autoDismiss(false)
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .show();
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }



    /*public static String getTimeStamp() {
        //return new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.US).format(new Date());
    }*/
}
