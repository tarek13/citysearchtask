package com.task.city.search.data.network;

import com.task.city.search.data.network.model.CityItemResponse;
import com.task.city.search.data.network.model.FacebookUserInfo;
import com.task.city.search.data.network.model.GoogleUserInfo;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface CitiesApis {

    @GET("lutangar/cities.json/master/cities.json")
    Observable<ArrayList<CityItemResponse>> getCities();

    @GET("v1/userinfo")
    Observable<GoogleUserInfo> getGoogleUserInfo(@Header("Authorization") String token);

    @GET("me")
    Observable<FacebookUserInfo> getFaceBookUserInfo(@Header("Authorization") String token, @Query("fields") String fields);
}
