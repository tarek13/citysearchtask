/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.task.city.search.ui.splash.presenter;

import com.task.city.search.data.model.UserInfo;
import com.task.city.search.ui.base.presenter.BasePresenter;
import com.task.city.search.ui.splash.interactor.ISplashInteractor;
import com.task.city.search.ui.splash.view.SplashView;
import com.task.city.search.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;



public class SplashPresenter<V extends SplashView, I extends ISplashInteractor>
        extends BasePresenter<V, I> implements com.task.city.search.ui.splash.presenter.ISplashPresenter<V, I> {

    @Inject
    public SplashPresenter(I mvpInteractor,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);



    }

    @Override
    public UserInfo getCurrentUser(){
        return  getInteractor().getPreferencesHelper().getCurrentUser();

    }




}
